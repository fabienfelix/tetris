package com.mooncreative;

import com.mooncreative.motors.GameEngine;
import com.mooncreative.motors.GameView;
import com.mooncreative.pieces.Barre;
import com.mooncreative.pieces.Carre;
import com.mooncreative.pieces.LGauche;
import com.mooncreative.pieces.Piece;
import com.mooncreative.pieces.SGauche;
import com.mooncreative.pieces.Te;

public final class AppDatas {

	private final int viewFPS = 30;
	private final int gameFpsMin = 5;
	private final int gameFpsMax = gameFpsMin * 3;
	private final int gridWidth = 10;
	private final int gridHeight = 20;
	private final int gridScareSize = 40;
	private final int bordure = 10;
	private final int windowXSize = gridWidth * gridScareSize + 2 * bordure;
	private final int windowYSize = (gridHeight + 3) * gridScareSize;
	private final int xLocationScore = windowXSize * 1 / 3;
	private final int yLocationScore = (int) ((gridHeight + 1) * gridScareSize * 1.1f);
	private final Piece[] pieceRandom = new Piece[] { new Barre(), new Te(), new Carre(), new LGauche(), new SGauche() };

	private boolean gameOver = false;
	private boolean pause = false;
	private boolean beginning = true;
	private boolean accelerating = false;
	private int gameFps = gameFpsMin;
	private int[] origine = { gridWidth / 2 - 3, -4 };
	private Piece[] tPieces = new Piece[gridWidth * (gridHeight + 10)];
	/**
	 * Tableau indiquant la pr�sence des pi�ces.
	 */
	private Boolean[][] gridState = new Boolean[gridWidth][gridHeight + 1];

	private int score;

	// Attribut static pour stocker la classe
	private static AppDatas INSTANCE;

	// Constructeur priv�
	private AppDatas() {
		this.score = 0;
		initialiserGridState();
	}

	// Ne peut �tre instanci�e qu'une seule fois
	public static AppDatas getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AppDatas();
		}
		return INSTANCE;
	}

	/**
	 * Remet gridState � false.
	 * 
	 * @see {@link #gridState}
	 */
	public final void initialiserGridState() {
		for (int i = 0; i < gridState.length; i++) {
			for (int j = 0; j < gridState[0].length; j++) {
				gridState[i][j] = false;
			}
		}
	}

	/**
	 * R��value la grille compl�te.
	 * 
	 * @see {@link #gridState}
	 */
	public final void actualiserGridState() {
		initialiserGridState();
		for (int i = 0; i < tPieces.length; i++) {
			if (tPieces[i] != null) {
				int x = tPieces[i].getCoordonnee().getX();
				int y = tPieces[i].getCoordonnee().getY();
				if (y > 0) {
					// System.out.println(x + " , " + y);
					gridState[tPieces[i].getCoordonnee().getX()][tPieces[i].getCoordonnee().getY()] = true;
				}
			}
		}
	}

	public final int getGameFPS() {
		return gameFps;
	}

	public final int getViewFPS() {
		return viewFPS;
	}

	public final int getGridWidth() {
		return gridWidth;
	}

	public final int getGridHeight() {
		return gridHeight;
	}

	public final int getWindowXSize() {
		return windowXSize;
	}

	public final int getWindowYSize() {
		return windowYSize;
	}

	public final int getScore() {
		return score;
	}

	public final void setScore(int score) {
		this.score = score;
	}

	public final Piece[] gettPieces() {
		return tPieces;
	}

	public final void settPieces(Piece[] tPieces) {
		this.tPieces = tPieces;
	}

	public final int getGridScareSize() {
		return gridScareSize;
	}

	public final Boolean[][] getGridState() {
		return gridState;
	}

	public final void setGridState(Boolean[][] gridState) {
		this.gridState = gridState;
	}

	public final int getBordure() {
		return bordure;
	}

	public final int[] getOrigine() {
		return origine;
	}

	public final void setGameFPS(int gameFPS) {
		this.gameFps = gameFPS;
	}

	public final boolean isAccelerating() {
		return accelerating;
	}

	public final void setAccelerating(boolean accelerating) {
		this.accelerating = accelerating;
	}

	public final int getGameFpsMin() {
		return gameFpsMin;
	}

	public final int getGameFpsMax() {
		return gameFpsMax;
	}

	public final boolean isGameOver() {
		return gameOver;
	}

	public final void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public final int getxLocationScore() {
		return xLocationScore;
	}

	public final int getyLocationScore() {
		return yLocationScore;
	}

	public final Piece[] getPieceRandom() {
		return pieceRandom;
	}

	public final boolean isPause() {
		return pause;
	}

	public final void setPause(boolean pause) {
		this.pause = pause;
	}

	public final boolean isBeginning() {
		return beginning;
	}

	public final void setBeginning(boolean beginning) {
		this.beginning = beginning;
	}

}
