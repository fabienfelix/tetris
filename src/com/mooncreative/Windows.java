package com.mooncreative;

import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import com.mooncreative.motors.GameEngine;
import com.mooncreative.motors.GameView;

public class Windows extends JFrame {

	// Attribut static pour stocker la classe
	private static Windows INSTANCE;

	// Ne peut �tre instanci�e qu'une seule fois
	public static Windows getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Windows();
		}
		return INSTANCE;
	}

	private AppDatas appDatas = AppDatas.getInstance();
	private GameView gameView = GameView.getInstance();
	private GameEngine gameEngine = GameEngine.getInstance();

	private Windows() {
		// On construis la fen�tre
		buildFenetre();
		processKeys();

	}

	private void buildFenetre() {
		// Swing s'occupe de la d�coration
		setDefaultLookAndFeelDecorated(true);
		// Le bouton close termine l'appliction
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// D�finir le titre
		setTitle("Tetris");
		// D�finir la taille
		setSize(appDatas.getWindowXSize(), appDatas.getWindowYSize());
		// On centre la fenetre sur l'�cran
		setLocationRelativeTo(null);
		// On interdit le redimensionnement de la fenetre
		setResizable(false);

		// On ajoute notre panel de widgets
		setContentPane(gameView);
		// setContentPane(buildContentPane());

		// On rend la fenetre visible
		setVisible(true);
	}

	private void processKeys() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				if (e.getID() == KeyEvent.KEY_PRESSED) {
					handleKeyPress(e.getKeyCode());
				}
				return false;
			}
		});
	}

	private void handleKeyPress(int keyCode) {

		if (appDatas.isBeginning()) {
			appDatas.setBeginning(false);
		} else {
			switch (keyCode) {
			case 32:// SPACEBAR
				
				appDatas.setGameFPS(appDatas.getGameFpsMax());
				//
				// appDatas.setAccelerating(!appDatas.isAccelerating());
				// if (appDatas.isAccelerating()) {
				// appDatas.setGameFPS(appDatas.getGameFpsMax());
				// } else {
				// appDatas.setGameFPS(appDatas.getGameFpsMin());
				// }
				break;
			case 37:// LEFT KEY
				gameEngine.deplacerPieceHorizontalement(false);
				break;
			case 38:// UP KEY
				gameEngine.tournerPiece(false);
				break;
			case 39:// RIGHT KEY
				gameEngine.deplacerPieceHorizontalement(true);
				break;
			case 40:// DOWN KEY
				gameEngine.tournerPiece(true);
				break;
			case 80:// P KEY
				appDatas.setPause(!appDatas.isPause());
				break;
			}
		}
	}

}
