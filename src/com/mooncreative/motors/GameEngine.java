package com.mooncreative.motors;

import java.util.Random;

import org.w3c.dom.css.ElementCSSInlineStyle;

import com.mooncreative.AppDatas;
import com.mooncreative.Coordonnees;
import com.mooncreative.TetrisThread;
import com.mooncreative.pieces.Barre;
import com.mooncreative.pieces.Carre;
import com.mooncreative.pieces.LGauche;
import com.mooncreative.pieces.Piece;
import com.mooncreative.pieces.SGauche;
import com.mooncreative.pieces.Te;

public final class GameEngine {

	private AppDatas appDatas;
	private Piece[] tPieces;
	private Boolean[][] gridState;
	private int gridHeight;
	private int gridWidth;
	private int[] xPosition = new int[4];

	// Attribut static pour stocker la classe
	private static GameEngine INSTANCE;

	// Constructeur priv�
	private GameEngine() {
		this.appDatas = AppDatas.getInstance();
		this.tPieces = appDatas.gettPieces();
		this.gridState = appDatas.getGridState();
		this.gridHeight = appDatas.getGridHeight();
		this.gridWidth = appDatas.getGridWidth();
		commencerJeu();
	}

	// Ne peut �tre instanci�e qu'une seule fois
	public static GameEngine getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GameEngine();
		}
		return INSTANCE;
	}

	public void run() {
		if (!appDatas.isPause() && !appDatas.isBeginning()) {
			testGameOver();
			abaisserPieces();
			verifierSiLignesPleines();
		}
	}

	private void abaisserPieces() {
		Piece piece;
		int x;
		int y;

		// on v�rifie si la place est libre dessous
		for (int i = 0; i < tPieces.length; i++) {
			piece = tPieces[i];
			if (piece != null && piece.isMoving()) {
				x = piece.getCoordonnee().getX();
				y = piece.getCoordonnee().getY();
				try {
					if (y >= gridHeight || !isNextPlaceEmpty(x, y)) {
						arreterMouvement();
						creerPieceComplete();
						return;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}

		// si la place est libre on descend
		for (int i = 0; i < tPieces.length; i++) {
			piece = tPieces[i];
			if (piece != null && piece.isMoving()) {
				x = piece.getCoordonnee().getX();
				y = piece.getCoordonnee().getY();
				// System.out.println(y);
				if (y < gridHeight && isNextPlaceEmpty(x, y)) {
					piece.getCoordonnee().setY(y + 1);
				}
			}
		}
	}

	/**
	 * Arr�te toute les pi�ces en mouvement et met � jour gridState.
	 * 
	 * @see {@link #gridState}
	 */
	private void arreterMouvement() {
		Piece piece;
		int x;
		int y;
		for (int i = 0; i < tPieces.length; i++) {
			piece = tPieces[i];
			if (piece != null && piece.isMoving()) {
				piece.setMoving(false);
				x = piece.getCoordonnee().getX();
				y = piece.getCoordonnee().getY();
				gridState[x][y] = true;
			}
		}
	}

	public void tournerPiece(boolean sensHoraire) {
		if (sensHoraire) {
			for (int i = 0; i < tPieces.length; i++) {
				Piece piece = tPieces[i];
				if (piece != null && piece.isMoving()) {
					int position = 0;
					position = piece.getPosition() + 1;
					// on retourne � la position 0 si on �tait � la position 3
					if (position > 3) {
						position = 0;
					}
					piece.setPosition(position);
					int xRelativeRotation = piece.getRotation(position, piece.getPlace(), 0);
					int yRelativeRotation = piece.getRotation(position, piece.getPlace(), 1);
					// System.out.println("x = "+xRelativeRotation+" ; y = "+
					// yRelativeRotation);
					piece.setCoordonnee(new Coordonnees(piece.getCoordonnee().getX() + xRelativeRotation,
							piece.getCoordonnee().getY() + yRelativeRotation));
				}
			}
		} else {
			tournerPiece(true);
			tournerPiece(true);
			tournerPiece(true);
		}
	}

	public void deplacerPieceHorizontalement(boolean versLaDroite) {
		int sens = -1;
		if (versLaDroite) {
			sens = 1;
		}

		// on teste les bordures verticales de la map
		for (int i = 0; i < xPosition.length; i++) {
			if (xPosition[i] == 0 && !versLaDroite) {
				return;
			}
			if (xPosition[i] == gridWidth - 1 && versLaDroite) {
				return;
			}
		}

		// on v�rifie s'il y a une pi�ce sur les cot�s
		for (int i = 0; i < tPieces.length; i++) {
			Piece piece = tPieces[i];
			if (piece != null && piece.isMoving()) {
				int x = piece.getCoordonnee().getX();
				int y = piece.getCoordonnee().getY();
				// Atention : java.lang.ArrayIndexOutOfBoundsException: -6
				if (gridState[x + sens][y]) {
					System.out.println("ici");
					return;
				}
			}
		}

		int j = 0;
		boolean coteLibre = true;
		for (int i = 0; i < tPieces.length; i++) {
			Piece piece = tPieces[i];
			if (piece != null && piece.isMoving()) {
				int x = piece.getCoordonnee().getX();
				int y = piece.getCoordonnee().getY();

				piece.setCoordonnee(new Coordonnees(x + sens, y));
				xPosition[j] = x + sens;
				j++;

			}
		}
	}

	/**
	 * @param x
	 *            Coordonn�e
	 * @param y
	 *            Coordonn�e
	 * @return true si la place dessous est vide
	 */
	private boolean isNextPlaceEmpty(int x, int y) {
		if (y > 0 && gridState[x][y + 1]) {
			return false;
		}
		return true;
	}

	private void testGameOver() {
		for (int x = 0; x < gridState.length; x++) {
			if (gridState[x][1]) {
				appDatas.setGameOver(true);
				break;
			}
		}
	}

	private void verifierSiLignesPleines() {
		boolean isFill;
		for (int y = 0; y < gridState[0].length; y++) {
			isFill = true;
			for (int x = 0; x < gridState.length; x++) {
				if (!gridState[x][y]) {
					isFill = false;
					break;
				}
			}
			// Si la ligne est pleine
			if (isFill) {
				actionsEffectueesSiLignePleine(y);
			}
		}
	}

	private void actionsEffectueesSiLignePleine(int ligneY) {
		// On supprime les pi�ces sur la ligne
		for (int i = 0; i < tPieces.length; i++) {
			if (tPieces[i] != null && tPieces[i].getCoordonnee().getY() == ligneY) {
				tPieces[i] = null;
			}
		}
		// On descend les pi�ces du dessus
		Piece piece;
		int y;
		for (int i = 0; i < tPieces.length; i++) {
			piece = tPieces[i];
			if (piece != null) {
				y = piece.getCoordonnee().getY();
				if (y < ligneY) {
					piece.getCoordonnee().setY(y + 1);
				}
			}
		}
		// on indique que les cases sont vides dans la grille
		appDatas.actualiserGridState();
		// on implemente le score
		appDatas.setScore(appDatas.getScore() + 1);
	}

	private void creerPieceComplete(Piece piece) {
		int[][] creation = piece.getCreation();
		for (int i = 0; i < creation.length; i++) {
			int j = 0;
			// On cherche j tel que tPieces[j] est null
			while (tPieces[j] != null && j < tPieces.length) {
				if (j == tPieces.length - 1) {
					System.out.println("*** Attention : tableau tPieces plein !");
				}
				j++;
			}
			tPieces[j] = (Piece) piece.createNew();
			tPieces[j].setPlace(i);
			int x = creation[i][0] + appDatas.getOrigine()[0];
			int y = creation[i][1] + +appDatas.getOrigine()[1];
			tPieces[j].setCoordonnee(new Coordonnees(x, y));

			xPosition[i] = x;
		}

		appDatas.setGameFPS(appDatas.getGameFpsMin());
	}

	/**
	 * Cr�er une pi�ce compl�te al�atoire compos�e de 4 sous pi�ce.
	 */
	private void creerPieceComplete() {
		int i = new Random().nextInt(appDatas.getPieceRandom().length);
		Piece piece = appDatas.getPieceRandom()[i];
		creerPieceComplete(piece);
		// creerPieceComplete(new SGauche());
	}

	public void commencerJeu() {
		appDatas.setScore(0);
		appDatas.setGameOver(false);
		creerPieceComplete();
	}

}
