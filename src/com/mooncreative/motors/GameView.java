package com.mooncreative.motors;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.mooncreative.AppDatas;
import com.mooncreative.pieces.Piece;

public final class GameView extends JPanel {

	private static final long serialVersionUID = 1L;

	private AppDatas appDatas;
	private Piece[] tPieces;

	private int x;
	private int y;
	private int windowXSize;
	private int windowYSize;
	private int gridScareSize;
	private int bordure;
	private final int xLocationScore;
	private final int yLocationScore;

	private final Font fontScore = new Font("Courier", Font.BOLD, 15);
	private final Font fontGameOver = new Font("Courier", Font.BOLD, windowXSize / 10);
	private final Font fontBeginning = new Font("Courier", Font.BOLD, 15);

	// Attribut static pour stocker la classe
	private static GameView INSTANCE;

	// Constructeur priv�
	private GameView() {
		this.appDatas = AppDatas.getInstance();
		this.tPieces = appDatas.gettPieces();
		this.windowXSize = appDatas.getWindowXSize();
		this.windowYSize = appDatas.getWindowYSize();
		this.gridScareSize = appDatas.getGridScareSize();
		this.bordure = appDatas.getBordure();
		this.xLocationScore = appDatas.getxLocationScore();
		this.yLocationScore = appDatas.getyLocationScore();
	}

	// Ne peut �tre instanci�e qu'une seule fois
	public static GameView getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GameView();
		}
		return INSTANCE;
	}

	public void paintComponent(Graphics g) {
		// fond blanc
		g.setColor(Color.WHITE);
		g.setColor(new Color(180, 180, 180));
		g.fillRect(0, 0, windowXSize, windowYSize);

		// Bordure
		g.setColor(Color.BLACK);
		// gauche
		g.fillRect(0, 0, bordure, windowYSize);
		// droite
		g.fillRect(appDatas.getGridWidth() * gridScareSize + bordure, 0, bordure, windowYSize);
		// bas
		g.fillRect(0, (appDatas.getGridHeight() + 1) * gridScareSize, windowXSize + 2 * bordure,
				appDatas.getGridHeight() * gridScareSize * 2);

		// Score
		g.setFont(fontScore);
		g.setColor(Color.WHITE);
		g.drawString("Score : " + appDatas.getScore(), xLocationScore,
				(appDatas.getGridHeight() + 1) * gridScareSize + 20);

		if (appDatas.isBeginning()) {
			// Ecrire text intro
			g.setFont(fontBeginning);
			g.setColor(Color.BLACK);
			g.drawString("*** TOUCHES ***", 60, 70);
			g.drawString("left : left arrow", 50, 100);
			g.drawString("right : right arrow", 50, 120);
			g.drawString("turn right : down arrow", 50, 140);
			g.drawString("turn left : up arrow", 50, 160);
			g.drawString("accelerate : space bar", 50, 180);
			g.drawString("pause : p", 50, 200);
			g.drawString("> Press any key to begin", 60, 230);
		} else if (appDatas.isGameOver()) {
			// Ecrire game over
			g.setColor(Color.RED);
			g.setFont(fontBeginning);
			g.drawString("GAME OVER !", windowXSize / 5, windowYSize / 2);
		} else {
			dessinerPieces(g);
		}
	}

	private void dessinerPieces(Graphics g) {
		for (Piece piece : tPieces) {
			if (piece != null) {
				x = piece.getCoordonnee().getX();
				y = piece.getCoordonnee().getY();
				g.setColor(piece.getColor());
				g.fillRect(x * gridScareSize + bordure, y * gridScareSize, gridScareSize, gridScareSize);
			}
		}
	}

}
