package com.mooncreative.pieces;

import java.awt.Color;

public class Te extends Piece {
	public Te() {
		this.color = new Color(15, 157, 232);
		this.rotation = new int[][][] { { { 0,-2 }, { 1,-1 }, { 2, 0 }, { 0, 0 } },
				{ { 2, 0 }, { 1, 1 }, { 0, 2 }, { 0, 0 } }, { { 0, 2 }, { -1, 1 }, { -2, 0 }, { 0, 0 } },
				{ { -2, 0 }, { -1, -1 }, { 0, -2 }, { 0, 0 } } };
		this.creation = new int[][] { { 1, -4 }, { 2, -4 }, { 3, -4 }, { 2, -3 } };
	}

	public Piece createNew() {
		return new Te();
	}

}
