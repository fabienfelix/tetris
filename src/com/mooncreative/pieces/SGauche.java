package com.mooncreative.pieces;

import java.awt.Color;

public class SGauche extends Piece {
	public SGauche() {
		this.color = new Color(150, 231, 90);
		this.rotation = new int[][][] { { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 2, -1 } },
				{ { -1, -1 }, { 0, 0 }, { -1, 1 }, { 0, 2 } }, { { 2, -1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } },
				{ { 0, 2 }, { -1, 1 }, { 0, 0 }, { -1, -1 } } };
		this.creation = new int[][] { { 1, -1 }, { 1, -2 }, { 2, -2 }, { 2, -3 } };
	}

	public Piece createNew() {
		return new SGauche();
	}

}