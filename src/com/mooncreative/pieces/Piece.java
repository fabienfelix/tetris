package com.mooncreative.pieces;

import java.awt.Color;

import com.mooncreative.Coordonnees;

public abstract class Piece {

	protected boolean moving = true;
	protected int place;
	protected int position = 0;
	protected Color color;
	protected int[][][] rotation;
	protected int[][] creation;
	// protected int[][] structure;
	protected Coordonnees coordonnee = new Coordonnees();

	public final Color getColor() {
		return color;
	}

	public final Coordonnees getCoordonnee() {
		return coordonnee;
	}

	public final void setCoordonnee(Coordonnees coordonnee) {
		this.coordonnee = coordonnee;
	}

	public final int getRotation(int position, int place,int coordon�e) {
		return rotation[position][place][coordon�e];
	}

	public final int[][] getCreation() {
		return creation;
	}
	
	public Piece createNew() {
		return null;
	}

	public final boolean isMoving() {
		return moving;
	}

	public final void setMoving(boolean moving) {
		this.moving = moving;
	}

	public final int getPlace() {
		return place;
	}

	public final void setPlace(int place) {
		this.place = place;
	}

	public final int getPosition() {
		return position;
	}

	public final void setPosition(int position) {
		this.position = position;
	}
	
	
	

}
