package com.mooncreative.pieces;

import java.awt.Color;

public class LGauche extends Piece {
	public LGauche() {
		this.color = Color.ORANGE;
		this.rotation = new int[][][] { { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 2, -1 } },
				{ { 0, -1 }, { -1, 0 }, { 0, 1 }, { 1, 2 } },
				{ { 1, -1 }, { 0, -2 }, { -1, -1 }, { -2, 0 } },
				{ { 0, 2 }, { 1, 1 }, { 0, 0 }, { -1, -1 } } };
		this.creation = new int[][] { { 1, -2 }, { 2, -2 }, { 2, -3 }, { 2, -4 } };
	}

	public Piece createNew() {
		return new LGauche();
	}

}