package com.mooncreative.pieces;

import java.awt.Color;

public class Carre extends Piece {
	public Carre() {
		this.color = new Color(192, 107, 220);
		this.rotation = new int[][][] { { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } },
				{ { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } }, { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } },
				{ { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } } };
		this.creation = new int[][] { { 1, -3 }, { 2, -3 }, { 2, -2 }, { 1, -2 } };
	}

	public Piece createNew() {
		return new Carre();
	}
}