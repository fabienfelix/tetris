package com.mooncreative;

import com.mooncreative.motors.GameEngine;
import com.mooncreative.motors.GameView;

public final class TetrisThread extends Thread {

	private GameEngine gameEngine;
	private GameView gameView;
	private AppDatas appDatas;
	private long threadPause = 10;

	private int viewFPS;

	private long time;
	private long oldViewTime;
	private long oldGameTime;
	private boolean draw = true;
	private static boolean actif = false;

	// Attribut static pour stocker la classe
	private static TetrisThread INSTANCE;

	// Ne peut �tre instanci�e qu'une seule fois
	public static TetrisThread getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new TetrisThread();
		}
		return INSTANCE;
	}

	private TetrisThread() {
		this.gameEngine = GameEngine.getInstance();
		this.gameView = GameView.getInstance();
		this.appDatas = AppDatas.getInstance();
		this.oldViewTime = System.currentTimeMillis();
		this.oldGameTime = System.currentTimeMillis();
		this.viewFPS = appDatas.getViewFPS();
		if (appDatas.getViewFPS() > appDatas.getGameFpsMax()) {
			this.threadPause = 1000 / appDatas.getViewFPS();
		} else {
			this.threadPause = 1000 / appDatas.getGameFpsMax();
		}
	}

	public void run() {
		while (actif) {
			time = System.currentTimeMillis();

			if (time - oldGameTime > 1000 / appDatas.getGameFPS() && !appDatas.isGameOver()) {
				// System.out.println("oldGameTime");
				this.gameEngine.run();
				oldGameTime = System.currentTimeMillis();
			}
			if (time - oldViewTime > 1000 / viewFPS) {
				// System.out.println("oldViewTime");
				this.gameView.repaint();
				oldViewTime = System.currentTimeMillis();
			}
			// if (appDatas.isGameOver()) {
			// this.gameEngine.run();
			// this.gameView.repaint();
			// arreter();
			// }
			try {
				Thread.sleep(threadPause + 1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void arreter() {
		actif = false;
	}

	public static void lancer() {
		actif = true;
	}
}